\section{Classifier}
Our results in \S\ref{usenix-analysis} led us to believe that a predictive model could be effective in reducing runtime prompts by predicting user decisions for most data accesses. More specifically, our goal is to predict user responses to permission request prompts based on their past decisions and behavior using a classifier. We gathered data across many users through a user study on PhoneLab and used this data to train and evaluate the classifier, based on their answers to our runtime prompts. Ideally, this classifier will predict a user decision to a permission request accurately, which we can use to allow or deny a permission request instead of prompting the user. This results in a permission model that has less prompting, annoyance, and risk of habituation for the user while still adhering to their privacy preferences. Currently, we train a single global model across all users; we hope to create more personalized models in future work.\\ \\
While we had some ideas for features to our classifier from our \textit{screen-on} and \textit{screen-off} model analysis, we needed to explore a much broader set of features while replacing some predictive variables. For example, the \textbf{userCode} variable caused a problem; training a classifier for each user would require large amounts of labeled data, generated through runtime prompts. To avoid the overwhelming habituation and user annoyance problem, we needed to determine metrics that could be measured at run-time to replace the \textbf{userCode} but still capture each user's uniqueness as determined by their individual privacy behaviors.

\subsection{Features} \label{feature-section}
The new features we proposed have two goals: help us infer personal user privacy preferences for each individual, and help understand context surrounding a user decision. The labeled dataset is provided through user responses to permission prompts during a large-scale user study on PhoneLab preceding the building of the classifier. We collect 33 feature groups, which sum to over 16,000 individual features, resulting in the most extensive instrumentation of the Android system to our knowledge. Due to space constraints, I only highlight the thought process behind feature generation and delve into some of the more interesting feature categories. The full list of instrumentation points feature groups can be found in the appendix.

\begin{table}
\centering
\begin{tabular}{|c|c|} \hline
\textbf{Feature} & \textbf{Setting Type} \\ \hline
User accesses security settings & Security \\ \hline
User actively modifies security settings & Security \\ \hline
Type of lockscreen & Security\\ \hline
Length of lockscreen password & Security\\ \hline
Is lockscreen password hidden & Security\\ \hline
User changes type of lock & Security \\ \hline
Location tracking on/off & Location \\ \hline
Granularity of location  & Location \\ \hline
NFC on/off & Misc \\ \hline
Developer options enabled/disabled & Misc \\
\hline\end{tabular}
\caption{User actions recorded in settings features.}
\label{tbl:settings-fts}
\end{table}

\subsubsection{Settings-Related Features}
An excellent source of preference-indicative information lies in phone settings, since those are directly controlled by the user, barring default initialization values. We focused on two main subsets of settings: security settings and location settings. We gathered information about each user's settings and extracted features from this. \\ \\
Table \ref{tbl:settings-fts} lists the subset of the data we collect from the settings panel. Security settings indicate privacy preferences directly, since a user can control access to her phone through this menu. Location settings help us better understand the user's location preferences; our previous study demonstrated that whether users considered location private varied greatly with the user and the context, so we expected this feature to be helpful in predicting user preferences regarding application use of location permissions. We also record NFC and developer options settings, since they are a means of exporting or importing data from the phone. To account for default settings, we record both the default values and current values of each setting for each user, so we know if these settings have been changed. We can compare these with their current values to detect which ones users actively changed.\\ \\
I personally instrumented all of the features mentioned in this section.

\subsubsection{User Behavior Features}
Many of our features directly focus on identifying user privacy-related behavioral habits of users; this subset of features is the most useful in creating a privacy preference ``profile'' for a user and clustering users by their similarities in privacy preferences. Many of these features are user actions taken on the device which do not directly relate to security, but help us profile the user's behavior patterns in general. These features fall into a few general categories:
\begin{itemize}
\item Screen Timeouts
\item Ringer Preferences
\item Phone Call Habits
\item Mobile Browser Habits
\item Camera/Picture Habits
\item Notification Habits
\item Two Factor Authentication
\end{itemize}
Our instrumentation indicates whether the screen turned off due to a timeout being reached, or if the user physically presses the lock screen button to turn off the screen. Our hypothesis is that a user physically locking their screen more often is more concerned about threats to their privacy. Ringer preferences and phone call habits imply how publicly a user carries on their communications. A user that keeps their phone ringer on loud essentially broadcasts incoming messages and phone calls to everyone in the vicinity, whereas a user keeping their phone on silent or vibrate does not. Similarly, speakerphone and headphone usage on phone calls can be used to infer the publicity of conversations. \\ \\
Mobile browsing habits may be a strong indicator of user privacy preferences. Some features that seem promising are the percentage of SSL-secured links that users visit, how often they encounter SSL warnings, and the percentage of incognito tabs they open as opposed to non-incognito tabs. We hypothesize that a user that visits a higher percentage of SSL-secured links, uses incognito tabs often, and does not ignore SSL warnings often will tend to be more conservative with their data. \\ \\
Picture-taking and uploading habits are associated with social media, thus we consider them as indicators of privacy preferences. Furthermore, this feature group easily demonstrates how one instrumentation point results in multiple features. Our instrumentation logs an event on each picture capture by the user, and when an application accesses the \texttt{MediaStore} to upload the picture. We transform these two types of event logs the following features: the number of pictures users take, the number of pictures they upload, the percentage of those pictures that are ``selfies'' (pictures of themselves), the percentage of pictures that are not selfies, etc. The feature generation for camera logs are based on our hypothesis is that the people that are more willing to share pictures, especially of themselves or family, are more likely to share other information as well. \\ \\
We also measure how often users get notifications from their applications and how quickly they respond to them. We believe that users that have much more communication are likely to be more social. Also, by differentiating which applications are sending the notifications, we can further infer actions users take on social media applications. Lastly, the use of two-factor authentication is one of the most obvious indicators we have of how much a user cares about their security. We hypothesize that a strong desire for security is tied to a strong desire for privacy, so features related to this seem quite promising as well.\\ \\
I personally instrumented all of the features highlighted in this section, except for mobile browser habits.

\subsubsection{Runtime Features}
Run-time features allow us to understand which activities the user is interacting with directly and create a timeline of the processes running on the system. These features provide us the greatest insight into exactly which user actions were taking place on the device. The three categories of run-time features that provide us with which applications and services were running on the device are memory prioritization, activity switches within an application, and sensitive permission requests. We also maintain a list of running processes and their visibility levels to the user.\\ \\
Memory prioritization directly indicates which applications are running in the foreground, in the background as services, or invisibly to the user. The process with the highest priority in memory is always a foreground activity that the user is directly interacting with. As the priority is lowered, we can know if an application is a visible service (such as Spotify, which retains a notification in the notification bar when it is playing music in the background), or an invisible service (such as Google Play services). Visibility is essential to the inferring expectedness of an information flow, and thus is critical to the context of a permission request \cite{wijesekera:remystified}. \\ \\
Activity switches within an application can be used to infer which actions a user is taking within the application itself. One of the fundamental limitations with instrumenting the Android operating system is that one cannot instrument third-party application code. Activity switches help us overcome this limitation by letting us infer which view of the application is in the foreground. For example, activity switching allows us to differentiate between when a user is browsing their Facebook news feed or viewing another Facebook user's profile. Using these switches, we can also compute how long a user spends in each activity on each application on her phone, letting us understand what the user tends to use her smartphone for. This helps us create a more accurate snapshot of the context under which a privacy decision was made. \\ \\
We have also selected a set of sensitive permission requests that have irreversible results, which can potentially be harmful to the user. Sensitive permission requests are monitored to give us a list of data access requests from applications. We can identify how much data applications seek (and are granted) in real-world usage situations. Furthermore, the prompts that we generate for sensitive requests to provide ground truth for the classifier are generated based on this category of features.

\begin{figure}
  \centering
  \includegraphics[height=5in, width=2in, keepaspectratio]{figures/user-prompt-example.png}
  \caption{Example of a prompt chosen by reservoir sampling that a user would see.}
  \label{fig:user-prompt}
\end{figure}

\subsection{Data Collection}
Training a classifier requires a large dataset. In our case, no dataset existed, so we had to generate our own. To achieve this, we deployed our highly instrumented operating system on PhoneLab, which installs our operating system on the phones of roughly 300 users \cite{phonelab}. Our experiment collected data for 5 weeks on Phonelab, collecting feature data as well as generating ground truth through a system-UI user prompt we created, as seen in Figure \ref{fig:user-prompt}.\\ \\
Our runtime user prompt gained ground truth on which sensitive permission requests that users considered privacy invasive; our prompt is similar in appearance to the ask-on-first-use prompt in Android Marshmallow, with the caveat that it did not only appear on first use. As shown in Figure \ref{fig:user-prompt}, we informed the user of a sensitive permission request and the requesting application, and asked if she would have denied it if given the opportunity. During the experiment, we made it clear to users that any answer would not affect their experience with the phone, and that their answer to the prompt would not actually affect information access patterns. \\ \\
We use a weighted reservoir sampling algorithm to determine which permission requests generate a prompt. The weights in our algorithm are chosen inversely to \textit{\{permission request, requesting application\}} combinations frequencies to ensure that we sample as diverse a set of combinations as possible. This ensures a ground truth sample for a large set of cases, allowing us to generalize and intialize our models well. We limit the phone to prompting the user once per day to minimize habituation.\\ \\
PhoneLab has the option for a user to opt out of an experiment at any time. Thus, not all 300 users participated in our experiment. In total, we had 204 users participate in our experiment, with 133 of them providing more than 20 days of data. We recorded over 176M events and 96M permission requests (one every 6 seconds per user). Users answered 4,670 runtime prompts over the course of the entire experiment, which translated directly to labeled data points with which to train our classifier.

\subsection{Offline Model}
Using the 4,670-point labeled dataset we generated in our PhoneLab experiment, we first wanted to determine which features were effective in predicting user decisions. During the feature selection process, we also evaluated various models and attempted to increase accuracy as much as we could. I worked heavily on all parts of the feature selection and modeling pipeline, so I will discuss the process in its entirety.

\subsubsection{Feature Selection}
We used a combination of regression tests determining statistically significant correlations and common machine learning techniques such as information gain values and regularization methods to determine which features were most predictive. Through this process, we hoped to gain insight into which user behaviors could be used to infer privacy preferences. In particular, we utilized logistic mixed effect regression, random forests, and regularization to reduce overfitting while emphasizing important features.\\ \\
We first confirmed if the results of our previous study, particularly pertaining to the predictive power of application visibility, still held. All models reported visibility as one of the most predictive factors, and logistic regression considered it statistically significant. We then analyzed how application usage correlated with users' decision rates. For each application used (not just installed) by at least 10 users, we measured usage time and checked to see if it significantly correlated with their denial rate for permission prompts through the Kendall rank correlation test. We found 6 applications for which a user's usage time significantly correlated with their denial rate across all permission prompts. The importance scores assigned by the random forest model confirmed the predictive power of the applications. The list of significant applications can be found in the appendix.\\ \\
We also used the Kendall rank correlation test for a few other features, which served as independent variables, with denial rate as the dependent variable. Some interesting features that correlated significantly are listed below.
\begin{itemize}
\item how often a user allows their phone screen to time out instead of actively locking it (measured as a ratio of timeouts:locks)
\item how often users access HTTPS links (measured as a ratio of HTTPS:non-HTTPS links)
\item the number of downloads the user initiates
\item the number of calls a user makes and receives per day (essentially the number of calls a user participates in daily)
\item the average call duration of a user
\end{itemize}
These features were also confirmed by the models as having high importance, and each of them helped increase accuracy, precision, and recall of our models. Interestingly, the effect size of each correlation was quite small; nearly all of them were less than 0.2. We believe this is partially because of our large sample size, consisting of 4,670 of decisions across 204 users. Furthermore, each feature has a large standard deviation. We hypothesize that the large variance in user smartphone interactions due to time of day and day of the week contribute to the large standard deviation, further lowering effect sizes \cite{Coe2002}. Specifically in the case of the application usage, we can also attribute the smaller effect size to the fact that none of the applications were used by all users.

\subsubsection{Model Evaluation}
\begin{table}
\centering
\begin{tabular}{|c|c|} \hline
\textbf{Feature} & \textbf{Feature Category} \\ \hline
Number of websites visited & Behavorial \\ \hline
Ratio of HTTPS-secured to non-HTTPS-secured sites & Behavioral \\ \hline
Ratio of websites requesting user location to those that did not request it & Behavorial\\ \hline
Number of downloads initiated by user & Behavorial\\ \hline
Type of screen lock (password, PIN, or pattern) & Behavorial\\ \hline
Number of screen unlocks daily & Behavorial \\ \hline
Total time spent unlocking & Behavorial \\ \hline
Ratio of screen locks to due timeout or user action & Behavorial \\ \hline
Number of phone calls daily (both made and received) & Behavorial \\ \hline
Amount of time spent on phone calls & Behavorial \\ \hline
Amount of time spent on silent mode on phone & Behavorial \\ \hline
Requesting application visibility & Runtime \\ \hline
Permission requested & Runtime \\ \hline
Denial rate per user for an \{app,perm,vis\} combination & Aggregate \\ \hline
Denial rate per user for an \{foreground app,perm,vis\} combination & Aggregate \\ \hline
\end{tabular}
\caption{Set of features used in SVM and RF classifier.}
\label{tbl:classifier-fts}
\end{table}

Improving our predictive model is an ongoing process, which we will likely continue to iterate in our future work. Currently, we have tried a logistic mixed-effect regression model for feature exploration, a random forest model, and a support vector machine (SVM) model for prediction. All reported accuracy and AUC values are averaged over five-fold cross-validation, with folds generated over each decision. This does not take temporal ordering into account meaning later decisions could be used to predict earlier decisions; we have yet to assess how this affects the accuracy of the model. We also used grid search to find optimal hyperparameter values for each model. The example confusion matrices are chosen from a single random fold, but are representative of the other four folds. \\ \\
The final set of features we used in our SVM and random forest models can be found in Table \ref{tbl:classifier-fts}. We group the features into three major categories: behavioral, runtime, and aggregate features. The behavioral features listed were the most predictive tended to relate to mobile browsing, phone calling, and screen locking habits. The predictive runtime features were the permission requested and the visibility of the requesting application. Interestingly absent is the requesting application itself, which did not have much predictive power. We believed this was a result of the numerous possible applications creating sparsity and reducing predictive power. To remedy this, we tried other features that would provide similar information, such as a boolean indicating if the requesting application was popular or highly used, but these features were also not predictive. The aggregate features, which computes the denial rate for permission prompts for each \{\textit{application, permission, visibility}\} combination, was inspired by the logistic mixed-effect regression model. We also compute the denial rate for different foreground applications, as it relates to decision context.\\ \\
We first used the logistic mixed-effect regression model for binary classification (keeping in mind that we use logistic regression as a classification model, not regression as the name would imply). Mixed effects account for two kinds of effects, inter-user and intra-user, and model these as fixed and random effects respectively. Thus, in cases of repeated measurements, such as our data collection process, they tend to do very well. The aggregate features mentioned above were our attempt to introduce intra-user effects for the SVM classifier, which does not inherently account for them. Essentially, without our aggregate feature, fixed-effect models like SVMs treat a single user's decisions as independent of each other.\\ \\
Our mixed-effects model had an accuracy of 92.3\% with an AUC of 0.92. We also used the same feature set on an SVM and random forest classifier. The SVM (with an RBF kernel) achieved an accuracy of 95.7\% with an AUC of 0.95, and the random forest had an accuracy of 91.8\% with an AUC of 0.91. Tables \ref{tbl:conf-matrix-glmer}, \ref{tbl:conf-matrix-svm}, and \ref{tbl:conf-matrix-rf} contain the confusion matrices for each of our models. The key point to notice is that some models tend to default towards denying, such as the logistic mixed-effect regression model. This means that the majority of their errors are when they choose to deny instead of allow a permission. We prefer these models to those that allow more, because falsely allowing data is more costly than denying it. Granted data cannot be redacted, but denied data can always be granted at a later time.\\ \\
\begin{table}[]
\begin{minipage}{0.45\textwidth}
\begin{tabular}{l|l|l}
 & Actual Deny & Actual Allow \\ \hline
 Predicted Deny & 504 & 50 \\ \hline
 Predicted Allow & 29 & 344 \\ 
\end{tabular}
\caption{The confusion matrix for logistic mixed-effect regression on a test set of 927.}
\label{tbl:conf-matrix-glmer}
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
\begin{tabular}{l|l|l}
 & Actual Deny & Actual Allow \\ \hline
 Predicted Deny & 536 & 18 \\ \hline
 Predicted Allow & 22 & 351 \\ 
\end{tabular}
\caption{The confusion matrix for our SVM classifier on a test set of 927.}
\label{tbl:conf-matrix-svm}
\end{minipage}
\centering
\begin{tabular}{l|l|l}
 & Actual Deny & Actual Allow \\ \hline
 Predicted Deny & 508 & 41 \\ \hline
 Predicted Allow & 35 & 343 \\ 
\end{tabular}
\caption{The confusion matrix for our random forest classifier on a test set of 927.}
\label{tbl:conf-matrix-rf}
\end{table}

Overall, the accuracy values we see are very encouraging, indicating that the machine learning approach could be promising in practice. Furthermore, through our behavioral features, it seems that passive behavioral data could be used to improve predictions and reduce prompting for the user. We have yet to evaluate these systems in full deployment, but these initial results indicate that this direction is one worth pursuing.
