\section{Understanding User Expectations} \label{usenix-paper}
The first step to being able to build a new permission model is to understand the nature of user expectations of privacy, quantify how many sensitive data accesses applications make, and determine which factors contribute strongly to user privacy decisions. To explore these questions, we conducted a field study of users in which we provided them with our custom instrumented version of Android to use for a week as their primary phone. Users then partook in an exit survey, in which we showed them various instances in which sensitive data accesses occurred. We asked the users whether those accesses were expected or surprising, and whether they would have blocked them given the ability.\\ \\
In this study, we focused on a group of \textit{sensitive permissions} (permissions that irreversibly leak user-sensitive data) recommended by Felt et al.\@ to be granted via runtime dialogs \cite{Felt2012b}. These 12 permissions compose about 16\% of all Android permission types, and are listed in Table \ref{tbl:perm-list}. Our follow-up work also focuses on these sensitive permissions, since we focus on improving runtime prompts.\\ \\
This section focuses on our field study published at the Usenix Security Symposium \cite{wijesekera:remystified}. I will concentrate primarily on my contributions to the work, while explaining the overarching experiment done by our team to provide context for the results and analysis.

\begin{table}[t]
\small
\center
\begin{tabular}{|l|l|}
\hline
\textbf{Permission Type} & \textbf{Activity} \\ \hline
\begin{tabular}[c]{@{}l@{}}WRITE\_SYNC\_\\ SETTINGS\end{tabular} & \begin{tabular}[c]{@{}l@{}}Change application sync settings \end{tabular} \\ \hline
\begin{tabular}[c]{@{}l@{}}ACCESS\_WIFI\_\\ STATE\end{tabular} & View nearby SSIDs \\ \hline
INTERNET & Open network sockets \\ \hline
NFC & Communicate via NFC \\ \hline
\begin{tabular}[c]{@{}l@{}}READ\_HISTORY\_\\ BOOKMARKS\end{tabular} & Read users' browser history \\ \hline
\begin{tabular}[c]{@{}l@{}}ACCESS\_FINE\_\\ LOCATION\end{tabular} & Read GPS location \\ \hline
\begin{tabular}[c]{@{}l@{}}ACCESS\_COARSE\_\\ LOCATION\end{tabular} & \begin{tabular}[c]{@{}l@{}}Read network-inferred location (i.e., cell tower and/or WiFi)\end{tabular} \\ \hline
\begin{tabular}[c]{@{}l@{}}LOCATION\_\\ HARDWARE\end{tabular} & Directly access GPS data \\ \hline
READ\_CALL\_LOG & Read call history \\ \hline
ADD\_VOICEMAIL & Add voicemails to the system \\ \hline
READ\_SMS & Read sent/received/draft SMS \\ \hline
SEND\_SMS & Send SMS \\ \hline
\end{tabular}
%\begin{flushleft}
\caption{The 12 permissions recommended by Felt et al.\@ to be granted via runtime dialogs \cite{Felt2012b}. These are the \textit{sensitive permissions} referred to throughout the paper.}
\label{tbl:perm-list}
%\end{flushleft}
\end{table}

\subsection{Methodology}
We broke the problem into two primary parts: measuring the frequency of permission requests, particularly sensitive ones, and understanding user reactions. We hoped to use these avenues to evaluate the viability of prompting the user on permission requests based on their frequency, and gain insight into user privacy preferences. Thus, we first instrumented Android 4.1 (Jellybean) to collect phone usage data and monitor data access endpoints. This instrumentation collected data during a 36-user field study on Nexus S devices lasting one week, culminating in an exit survey.

\subsubsection{Data Collection}
For every protected request, the Android operating system checks during runtime whether an application was granted a permission at install-time in its manifest. We added a logging framework to the platform to determine when requests were made to the system, allowing us to record each instance where an application received resources. Our logging system was split into many \texttt{Producers} and a single \texttt{Consumer}.\\ \\
\texttt{Producers} were placed throughout the platform in the many places that permission requests are made. A key point we monitored was the \texttt{checkPermission()} call in the \texttt{Context} implementation to give us access to the names of specific functions called from user-space applications. We also instrumented the \texttt{ContentProvider} class which facilitates application access to structured data (e.g., Contacts, Calendar) and the permission checks involved with \texttt{Intent} transmission. \texttt{Intents} allow data transfer between applications when activities are about to start in the receiving application. \texttt{Producers} were further used to monitor the \texttt{Binder} IPC mechanism, which allow applications to communicate with the Android system. This instrumentation produced the most logs, as it monitored calls to Android's Java API, which all applications use heavily.\\ \\
I primarily worked on the \texttt{Consumer} with another teammate. The \texttt{Consumer} consolidated logs from the \texttt{Producers} placed in various parts of the platform. These logs were written to internal storage, since the Android platform cannot write to external storage. Internal storage tends to be limited; on our Nexus S devices, it totaled to 1GB. This space was also shared with installed applications, requiring us to be very careful with space usage. We compressed log data every two hours. When this log data was shipped to our server (daily, when the user connected to WiFi), it was deleted from phone storage to further preserve space. The average compressed log file totaled to 108KB consisting of roughly 9,000 events over 2 hours.\\ \\
The \texttt{Consumer} also stored context information and metadata for each logged event. An event was a granted permission request, and each event log contained the following information:
\begin{itemize}
\item \textbf{Timestamp:} The time when the permission request occured.
\item \textbf{Permission name:} The permission requested.
\item \textbf{Application name:} The application requesting the permission.
\item \textbf{API method:} The calling method that resulted in the permission check. This could be used to infer specifically what data the application requested.
\item \textbf{Visibility:} Whether the requesting application was visible to the user at request time. An application could be in any of four visibility states: running as the foreground process that the user was directly interacting with, running as a foreground process in the background for multitasking purposes (e.g., when a user switches applications and \texttt{onPause()} is called), running as a service with user awareness (e.g., sound, notifications), and running as a service without user awareness.
\item \textbf{Screen Status:} Whether the phone screen was on or off.
\item \textbf{Connectivity:} Whether the phone was connected to WiFi at the time, making data transfer possible.
\item \textbf{Location:} This was the user's last cached physical location; we did not directly query the GPS to preserve battery.
\item \textbf{View:} The name of the visible screen as defined by the XML DOM used to render UI in Android. This could be used to infer what the user was interacting with on the screen.
\item \textbf{History:} Applications the user interacted with before the permission request.
\item \textbf{Path:} In the event that \texttt{ContentProvider} was accessed for data stored on the phone, we recorded the path of the resource requested.
\end{itemize}
From these elements, I specifically worked on recording permission name, application name, screen status, connectivity, and view.\\ \\
To maintain low system performance overhead, the \texttt{Consumer} also had rate-limiting logic. After 10,000 requests for any given \textit{\{application,permission\}} pair, it checked to see if it exceeded 1 log per 2 seconds for that pair. If so, it recorded future requests for that pair with probability 0.1, and made a note to allow us to extrapolate during analysis to recover actual counts.

\subsubsection{User Study}
\begin{figure*}[t!]
\begin{subfigure}[t]{0.35\textwidth}
\centering
\includegraphics[width=2.5in,height=2.5in]{figures/scnshotone.png}
\caption{Participants first establish awareness of the permission request based on the screenshot, and answered questions about context.}
\label{subfig:exit-survey-p1}
\end{subfigure}
\hfill
\begin{subfigure}[t]{0.55\textwidth}
\centering
\includegraphics[width=3.6in,height=2.5in]{figures/scnshottwo.png}
\caption{Participants then saw the resource accessed, and stated if they expected the permission and whether it should be blocked.}
\label{subfig:exit-survey-p2}
\end{subfigure}
\label{fig:exit-survey}
\caption{Exit Survey Application}
\end{figure*}

We recruited participants online in October 2014 through Craigslist by posting in the ``et cetera job'' section. We titled the listing ``Research Study on Android Smartphones'', explaining generally that the study was about how people interact with their smartphones. To avoid priming users, we avoided any mention of security or privacy. For each user, the study involved a 30-minute setup phase, a one week period where the user used our experimental phones as their primary mobile device, and an in-person exit survey lasting between 30 to 60 minutes.\\ \\
Potential participants were directed to download a mobile application that I developed. The application screened them to determine eligibility. Users would answer a short demographic survey on the application to ensure they were above 18 years of age, while the application determined their cell phone carrier, cell phone model, and installed applications. This information was important because our experimental Nexus S phones only could achieve 3G speeds on T-Mobile smartphones, and we checked the phone model to ensure that their SIM card was compatible with our Nexus S phones. The list of applications allowed us to pre-load the experimental phone we provided them with the applications they used to allow for a smooth transition to our phones, while minimizing setup time for them.\\ \\
48 users made it through the application screening process, and arrived at our setup session. Here we screened 8 users out because their phones were MetroPCS (the screening app could not discern between T-Mobile and MetroPCS) and thus incompatible with our experimental phones. All 48 users were provided with \$35 gift cards for attending the setup session, and 40 of them were given phones to use for a week. The setup session involved moving their SIM cards into the experimental phones, installing any paid applications (we only pre-installed free applications), and setting up their Google account on the phone to sync data and contacts. We had to manually sync the data if it was not linked to their Google account. \\ \\
During the week, our logging system recorded usage data as well as sampled screenshots to provide context for questions posed to users during the exit survey, as seen in Figure \ref{subfig:exit-survey-p1}. At the end of the week, 36 users returned our experimental phones, and they were provided with another \$100 gift card after the exit survey. We then flashed the phones to delete all user data from them. I personally conducted the setup phase and exit survey for half of our participants.

\subsubsection{Exit Survey}
We conducted the exit survey in a private room on a computer, with a researcher present to answer any questions without compromising user privacy. We did not view screenshots unless the participant gave us permission to do so. The survey was broken into three components: \textit{screenshot-based questions}, \textit{screen-off questions}, and \textit{general personal privacy preferences}.\\ \\
Screenshot-based questions first asked users to explain what they were doing on the phone (Figure \ref{subfig:exit-survey-p1}) in an open-ended response and asked them to guess which permission was being requested based on the displayed screenshot to set context. Permissions were displayed in simple English based on what resource they accessed. This first portion focused on understanding user expectations. The second portion (Figure \ref{subfig:exit-survey-p2}) revealed the accessed permission, and asked users how much they expected this access on a five-point Likert scale. We then asked if they would deny the permission if they had the option, followed by an open-ended question inviting them to explain their reasoning. Lastly, we asked for permission to view the screenshot. This process was repeated for between 10 and 15 screenshots for each user, determined through our \textit{weighted reservoir sampling} algorithm. Reservoir sampling allowed us to choose a subset of items from a large set of unknown size, such as the number permission requests our users would encounter. We utilized weighting to ensure that applications that requested fewer permission requests would still be represented in our dataset.\\ \\
The screen-off questions focused on understanding user expectations about protected resource accesses when users were not actively using their phone. Since the screen was off, there was no context for what they were doing. We asked users whether they would deny permission requests for 10 sampled \{application, permission\} pairs, and how expected these behaviors were, similarly to the screenshot-based questions.\\ \\
To understand general personal privacy preferences, we had also users answer two privacy scales: Buchanan et al.'s Privacy Concern Scale (PCS) \cite{BuchananPCS} and Malhotra et al.'s Internet Users Information Privacy Concerns (IUIPC) scale \cite{MalhotraIUIPC}.\\ \\
Three researchers, including me, independently coded the 423 responses for each of the open-ended questions. Before consensus, we disagreed on 42 responses for a 90\% inter-rater agreement rate. Given the 9 possible codings per response, Fleiss' kappa yielded 0.61, indicating substantial agreement.

\subsection{Results}
We logged a total of 27M resource requests across more than 300 applications during the week-long period, which is equivalent to 100,000 requests per user each day. 60\% of these requests were made while the screen was off, and an additional 15.1\% were done by invisible background applications or services, for a total of 75.1\% of requests invisible to the user. We focus on these requests, since they most likely defy user expectations, according to our study.

\subsubsection{Invisible Permission Requests}
\begin{table}[t]
\centering
\small
\begin{minipage}{0.45\textwidth}
\begin{tabular}{|l|r|}
\hline
{\bf Permission} &{\bf Requests} \\ \hline
ACCESS\_NETWORK\_STATE & 31,206 \\ \hline
WAKE\_LOCK & 23,816 \\ \hline
ACCESS\_FINE\_LOCATION & 5,652 \\ \hline
GET\_ACCOUNTS & 3,411 \\ \hline
ACCESS\_WIFI\_STATE & 1,826 \\ \hline
UPDATE\_DEVICE\_STATS & 1,426 \\ \hline
ACCESS\_COARSE\_LOCATION & 1,277 \\ \hline
AUTHENTICATE\_ACCOUNTS & 644 \\ \hline
READ\_SYNC\_SETTINGS & 426 \\ \hline
INTERNET & 416 \\ \hline
\end{tabular}
\begin{flushleft}
\caption{The most frequently requested permissions by invisible applications and services per user/day.}
\label{tbl:so_perm}
\end{flushleft}
\end{minipage}
\hfill
\begin{minipage}{0.45\textwidth}
\centering
\small
\begin{tabular}{|l|r|}
\hline
{\bf Application} & {\bf Requests} \\ \hline
Facebook & 36,346 \\ \hline
Google Location Reporting & 31,747 \\ \hline
Facebook Messenger & 22,008 \\ \hline
Taptu DJ & 10,662 \\ \hline
Google Maps & 5,483 \\ \hline
Google Gapps & 4,472 \\ \hline
Foursquare & 3,527 \\ \hline
Yahoo Weather & 2,659 \\ \hline
Devexpert Weather & 2,567 \\ \hline
Tile Game(Umoni) & 2,239 \\ \hline
\end{tabular}
\begin{flushleft}
\caption{The applications making the most permission requests while running invisibly, per user/day.}
\label{tbl:so_app}
\end{flushleft}
\vspace{-1em}
\end{minipage}
\end{table}

A total of 20.3M (75.1\%) requests were made with no visual cues to the user. The breakdown of all permission requests, based on our defined visibility levels, is as follows:
\begin{itemize}
\item \textbf{Visible foreground applications} constituted 12.04\% of requests.
\item \textbf{Visible background services} constituted 12.86\% of requests.
\item \textbf{Invisible background services} constituted 14.40\% of requests.
\item \textbf{Invisible background applications} constituted 0.70\% of requests.
\item Activity while the \textbf{screen was off} constituted 60.0\% of requests.
\end{itemize}
We analyzed the invisible permissions most frequently requested, and the applications that most frequently requested those permissions. ACCESS\_NETWORK\_STATE, the most popular permission, was requested roughly once every 3 seconds. Applications use this to check network connectivity. More concerning were the location data accesses, which can be requested via the ACCESS\_FINE\_LOCATION, ACCESS\_COARSE\_LOCATION, and ACCESS\_WIFI\_STATE (using WiFi SSIDs) permissions. Tables \ref{tbl:so_perm} and \ref{tbl:so_app} show this data, normalized per user per day.\\ \\
Location data accesses also strongly violated contextual integrity. Contextual integrity requires users to be aware of information flows to deem them appropriate, however less than 1\% of location requests were made by visible applications or display a GPS icon in the notification bar. This is because the GPS icon only appears when an application accesses the GPS sensor. 66.1\% of location requests used the \texttt{TelephonyManager} to determine location from cell tower information, 33.3\% used WiFI SSIDs, and used a built-in location provider the remaining 0.6\% of the time. Of this 0.6\%, the GPS sensor was only accessed 6\% of the time, because the majority of queries were to the cached location provider. Thus, in total, roughly 0.04\% of location requests displayed a GPS icon.\\ \\
Another interesting juxtaposition is comparing screen off requests with screen on requests. Harbach et al.\@ posit that users have their phone screen off 94\% of the time \cite{Harbach2014b}. Since only 60\% of permission requests occur during this time, it seems that permission request frequency drops when users are not actively using their phone. However, some applications such as Microsoft Sky Drive and Brave Frontier Service actually make more requests while the user is not interacting with their phone. These applications primarily request the ACCESS\_WIFI\_STATE or INTERNET permissions. We hypothesize that these applications were performing data transfer or synchronization tasks, but checking this would require examining the application source code.\\ \\
Lastly, invisible applications also read stored SMS messages 125 times per user/day, read browser history 5 times per user/day, and accessed the camera once per user/day. While these may not all be privacy violations, they do violate contextual integrity as the user is likely unaware of them.

\subsubsection{Request and Data Exposure Frequency}
Felt et al. recommended allowing benign data accesses, and only prompting for higher risk irreversible data requests. They classify the prompt-worthy data requests into the following categories, which are encompassed by the 12 sensitive permissions:
\begin{itemize}
\item Reading location information
\item Reading browser history
\item Reading SMS messages
\item Sending premium SMS messages (those that incur charges), or spamming the user's contact list with SMS messages
\end{itemize}

91 of 300 (30.3\%) applications requested these permissions, and these requests happened an average of 213 times per user/hour (roughly every 20 seconds). However, not all of these requests resulted in sensitive data being read or modified. We could differentiate between the cases which exposed data and those that didn't based on the called function name in the logs. The differentiation with examples is discussed next.
For example, \texttt{getWifiState()} only reveals whether WiFi is on, but \texttt{getScanResults()} returns a list of nearby SSIDs. The majority of location requests were to \texttt{getBestProvider()}, which returns the optimal location provider based on application needs, and not actually location data. Similarly, most requests for READ\_SMS requested SMS store information rather than actual SMS messages (eg. \texttt{renewMmsConnectivity()}). However, every request to SEND\_SMS actually sent an SMS. In browser history, functions reorganizing directories (eg. \texttt{addFolderToCurrent()}) did not expose data, while looking at visited URLs through \texttt{getAllVisitedUrls()} would.\\ \\
Overall, 5,111 of 11,598 (44.3\%) sensitive permission requests exposed user data. A breakdown of data accesses can be seen in Table \ref{tbl:data_exposed_breakdown}. Limiting runtime prompts to only these cases is still infeasible; this would cause a prompt nearly once every 40 seconds. Further solutions are discussed in \S\ref{usenix-analysis}.

\subsubsection{User Expectations}
\begin{table*}[t]
\center
\small
\begin{tabular}{|l||r|r||r|r||r|r|}
\hline
\textbf{Resource} & \multicolumn{2}{|c||}{\bf Visible} & \multicolumn{2}{|c||}{\bf Invisible} & \multicolumn{2}{|c|}{\bf Total}\\
& \textbf{Data Exposed} & \textbf{Requests} & \textbf{Data Exposed} & \textbf{Requests} & \textbf{Data Exposed} & \textbf{Requests}\\ \hline
%Location & 681 & 1,939 & 2,404 & 6,929 & 3,085 & 8,868 \\ \hline
%SSIDs  & 77 & 266 & 1,477 & 1,826 & 1,554 & 2,092 \\ \hline
Location & 758 & 2,205 & 3,881 & 8,755 & 4,639 & 10,960 \\ \hline
Read SMS data  & 378 & 486 & 72 & 125 & 450 & 611 \\ \hline
Sending SMS  & 7 & 7 & 1 & 1 & 8 & 8 \\ \hline
Browser History  & 12 & 14 & 2 & 5 & 14 & 19 \\ \hline
{\bf Total} & 1,155 & 2,712 & 3,956 & 8,886 & 5,111 & 11,598\\ \hline
\end{tabular}
\begin{flushleft}
\caption{The sensitive permission requests (per user/day) when requesting applications were visible/invisible to users. ``Data exposed'' refers to the subset of permission-protected requests that resulted in sensitive data being accessed.}
\label{tbl:data_exposed_breakdown}
\end{flushleft}
\end{table*}
Our exit survey collected 673 participant responses ($\approx$19 per participant) for \{application,permission\} pairs. Of these, 423 were screenshot-based (\textit{screen-on} requests) and 250 occurred while the screen was off (\textit{screen-off} requests). From the screenshot-based requests, 243 screenshots were taken when the requesting application was also in the foreground, and the other 180 screenshots were from invisible applications. Our reservoir sampling algorithm also attempted to diversify by the \{application,permission\} pairs as much as possible.\\ \\
Of the 36 participants, 80\% (30 participants) said they would have blocked at least one permission request. In all, participants wanted to block 149 (35\%) of all 423 screen-on requests. When participants rated requests for how expected they were (5-point Likert scale with higher number meaning more expected), allowed requests averaged 3.2, while blocked requests averaged 2.3. This supports the notion of contextual integrity, since users tended to block requests which defied their expectations. Furthermore, when queried for their reasons for wanting to block those requests, two themes emerged: (1) the request did not pertain to application functionality in their eyes, (2) the request involved resources they were uncomfortable sharing. In fact irrelevance to application functionality was the reason for 79 (53\%) of the 149 permissions users wanted to block. Privacy concerns, particularly sensitive information such as SMS, pictures, and conversations, were cited as the reason for denying 49 (32\%) of the 149 "blocked" permission requests. Conversely for allowed requests, users cited convenience (10\% of allowed requests) and a lack of sensitive data involved (21\% of allowed requests) as the reason for requests to proceed.\\ \\
We wanted to explore how certain factors related to user decisions to block or allow permission requests based on user responses. I personally performed many of the statistical tests, confirmed the ones I didn't perform, and worked heavily on the data analysis portion. Our results are summarized below.
\begin{itemize}
\item \textbf{Effect of Correctly Identifying Permissions on Blocking:} Of the 149 cases where participants wanted to block permission requests, they were only able to correctly state what permission was being requested 24\% of the time; whereas when wanting a request to proceed, they correctly identified the requested permission 44\% (120 of 274) of the time. However, Pearson’s product-moment test on the average number of blocked requests per user and the average number of correct answers per user did not yield a statistically significant correlation (r=$−0.171$, p$<$$0.317$).
\item \textbf{Effect of Visibility on Expectations:} Looking only at the \textit{screen-on} responses, we noted that users had an average expectation scale value of 3.4 for the 243 visible permission requests and 3.0 for the 180 invisible requests. Wilcoxon' signed-rank test with continuity correction revealed a statistically significant difference between the expectation values for the two groups (V=$441.5$, p$<$$0.001$). Thus, users expected visible requests much more than invisible requests.
\item \textbf{Effect of Visibility on Blocking:} We calculated the percentage of request denials for each participant, for both visible and invisible requests. Wilxcoxon's signed-rank test with continuity correction resulted in a statistically significant difference (V=$58$, p$<$$0.001$). Users were much more likely to deny invisible requests than visible ones.
\item \textbf{Effect of Privacy Preferences on Blocking:} We used Spearman's rank test to determine that there was no statistically significant correlation ($\rho$=$0.156$, p$<$$0.364$) between users' privacy scale (both PCS and IUIPC) scores and their desire to block permission requests. This result was not surprising, as previous studies have demonstrated a difference between stated privacy preferences and actual privacy behaviors \cite{Acquisti05}.
\item \textbf{Effect of Expectations on Blocking:} This directly related to evaluating contextual integrity as a good model to follow while analyzing user preferences. We calculated average Likhert scores for user expectations for a request and the percentage of requests they wanted to block. Pearson's product-moment test resulted in a statistically significant negative correlation (r=$-0.39$, p$<$$0.018$), indicating that unexpected requests were more likely to be denied.
\end{itemize}

\subsection{Analysis} \label{usenix-analysis}

Based on our field study results, we have shown that prompting on every data exposure is infeasible. However, there is also a dire need for better permission control mechanisms, as 80\% of users indicated at least one violation of contextual integrity. We evaluate ask-on-first-use prompts, recommend a new version of ask-on-first-use prompting, and provide a new direction for reducing runtime prompts below.

\subsubsection{Feasibility of Runtime Prompts}
We first wanted to evaluate the feasibility of runtime prompts, as Felt et al. recommended for the sensitive permissions in Table \ref{tbl:perm-list}, both in terms of accuracy and number of prompts. We used this to evaluate ask-on-first-use using the traditional \{application,permission\} pair (in iOS, since Android M had not launched at the time of our study), and a slightly modified version of ask-on-first-use in which we use a \{application,permission,visibility\} triplet.\\ \\
Based on our study data, users would see an average of 34 (ranging from 13 to 77, $\sigma=11$) runtime prompts in one week on a new phone if they were queried on first use of the triplet. By filtering out requests that do not expose data, we can reduce this number to 29 prompts. 21 (72\%) of the 29 prompts are for location data, which the iOS ask-on-first-use model already prompts for. There has been no evidence of user habituation or annoyance with the iOS model, indicating that the 8 extra prompts our system would induce would likely not introduce significantly more burden on the user. However, the accuracy of the policy increases greatly with the triplet. Looking at cases in our study where a user was prompted for the same \{application,permission\} pair twice, we evaluated whether the user's first decision to block agreed with their subsequent decisions. The ask-on-first-use pair policy had a 51.3\% agreement rate, while the triplet policy had a 83.5\% agreement rate.

\subsubsection{User Modeling}
We constructed several statistical models to examine whether users’ desire to block certain permission requests could be predicted using the contextual data that we collected. We wanted to evaluate if a classifier could be used to predict user preferences, where the only runtime prompts would be those where the classifier had low confidence in its decision. Thus, we constructed statistical models (mixed effect binary logistic regression models) to see if we could predict a user's desire to block a permission request. Our models set the response variable to be the user’s choice of whether to allow or block the permission request. Our predictive variables consisted of contextual information that would be available during runtime: a user ID variable representing the unique user (\textbf{userCode}), permission type (\textbf{perm}), requesting application (\textbf{app}), and visibility of the requesting application (\textbf{vis}).\\ \\
We found that creating separate models for the \textit{screen-on} data and \textit{screen-off} data resulted in much better fits for each model. For each model, we built two classifiers and evaluated their accuracy using 5-fold cross-validation, where our exit survey data served as ground truth. We tested each possible subset of our predictive variables with the model, using Akaike Information Criterion (AIC) and Bayesian Information Criterion (BIC) to judge goodness-of-fit, to determine which variables provided the greatest insight. We found that (\textbf{vis}, \textbf{app}, \textbf{userCode}) had the best fit for the \textit{screen-on} model, while (\textbf{perm}, \textbf{app}, \textbf{userCode}) provided the best fit for the \textit{screen-off} model. The predictive power of the \textbf{userCode} variable also revealed that each user's individual privacy preferences play a large role in their decision to allow or deny permission requests. \\ \\
We also provide some preliminary analysis using receiver operating characteristic (ROC) plots, which evaluate the tradeoffs between true-positive and false-positive rates. We computed the area under the ROC curve (AUC) for each model, and compared it to the random baseline of 0.5, with a maximum possible AUC of 1, indicating perfect classification. The \textit{screen-on} classifier AUC was 0.7 and the \textit{screen-off} classifier AUC was 0.8. This led us to believe that a classifier had a good chance of being able to predict user decisions, which inspired our next work.