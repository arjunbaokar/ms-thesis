\newgeometry{top=1in,bottom=1in,right=1in,left=1in}


\clearpage
\vspace*{\fill}
\begin{center}
\begin{minipage}{\textwidth}

\begin{abstract}
Smartphones contain a large amount of highly personal data, much of it accessible to third-party applications. Much of this information is safeguarded by a permission model, which regulates access to this information. This work primarily focuses on improving the Android permission model, which is known to have notoriously large amounts of sensitive data leakage, but many of its findings can be analogously applied to other mobile operating systems.\\ \\
We evaluate the two currently employed Android permission models: ask at \textit{install-time} and \textit{ask-on-first-use} to determine if they fulfill user expectations of privacy, and find that this is not the case for either model. We analyze the different facets that comprise user expectations and recommend a better mechanism to satisfy these expectations without excess effort from the user. This mechanism incorporates the contextual nature of privacy into the permission-granting process through the use of a machine learning classifier. \\ \\
We contribute the most extensive instrumentation of the Android operating system targeting user behavior and related runtime states to our knowledge, spanning across nearly 40 classes in the Android platform. This instrumentation allows us to utilize user behavior and system-level features to determine context for permission requests. The data from this instrumentation is used to generate features for the classifier. We evaluate the classifier on a large labeled dataset we collect from over 200 users using our modified operating system, and recommend ways to employ such a system in the real world based on our analysis.
\end{abstract}


\end{minipage}
\end{center}
\vfill % equivalent to \vspace{\fill}
\clearpage


\section{Introduction}

Smartphones have grown to serve billions of people, outstripping their desktop predecessor handily. In many places around the world, smartphones serve as the primary computing device for users. Android and iOS are the two most popular mobile operating systems, accounting for over 1.5 billion users \cite{Newman2014}, over 2.5 million applications, and 250 billion app downloads \cite{Sims2015}. As the popularity of the platforms and smartphones has grown, so has concern over user privacy.\\ \\
Android, the primary platform we examined, employs a permission model to regulate application access to data. Before Android 6.0 (Marshmallow), the model employed an ask-on-install policy, where the only way to deny a permission was to not install the application. Android Marshmallow and iOS employ an ask-on-first-use policy, which asks a user to make a decision the first time an application accesses sensitive data \cite{androiddev:sysperms}. This is in the form of a system prompt at runtime, which the user must respond to. However, users and developers often do not understand these permission models \cite{felt:androidpermissions}. Furthermore, it is not known if the prompts focus on data accesses that users find concerning. It is also unclear if the user decision for an ask-on-first-use prompt is consistent across subsequent requests of the same application requesting the same data \cite{wijesekera:remystified}. \\ \\
There has been some work done on creating systems to protect users from data exposure, either through static analysis of applications, dynamically enforcing an access policy specified by the user \cite{almohri:droidbarrier, Hornyack2011}, or dynamic taint tracking \cite{enck:taintdroid}. However, the static analysis systems do not provide real-time protection from sensitive data exposure, and the existing dynamic systems do not work for all applications and require copious configuration effort from the user, which the authors acknowledge is highly inconvenient for the average user. Thus, we hope to create a solution which is backwards-compatible for all applications, requires minimal effort from the user, and preserves privacy by monitoring permission requests in real-time. \\ \\
We explore the fundamental question of how smartphone platforms can empower users to control their information without added burden. Some facets of this question involve determining the feasibility and effectiveness of runtime prompting, as well as understanding users' mental models of privacy in the context of mobile devices.\\ \\
To address issues with current models, we draw on Nissenbaum's theory of \textit{contextual integrity}, which posits that privacy violations occur when information streams defy user expectations \cite{Nissenbaum2004}. For example, a navigation application requesting location data follows contextual norms, while Angry Birds' need to request location is less obvious to the user. Using the contextual integrity notion, we hope to increase the efficacy of current permission systems by focusing on sensitive permission requests that are likely to defy user expectations to reduce the number of prompts users have to respond to.\\ \\
We propose a new permission granting mechanism which utilizes machine learning to infer an individual user's privacy preferences and evaluate its feasibility. By leveraging inference, we want to avoid excessively prompting the user, which leads to habituation. Overall, this mechanism's goal is to be able to make contextual decisions for each individual user as they would with less user burden.


\subsection{Threat Model}
We assume that the platform itself, the Android operating system in this case, is trusted. The new permission model we propose protects users from applications they have installed that request permissions and sensitive information through the standard mechanism dictated by the operating system. Malware or exploits that subvert the operating system or the security monitor guarding permissions are outside the scope of our system.
