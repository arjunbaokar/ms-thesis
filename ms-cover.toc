\contentsline {section}{\numberline {1}Introduction}{4}
\contentsline {subsection}{\numberline {1.1}Threat Model}{5}
\contentsline {section}{\numberline {2}Understanding User Expectations}{5}
\contentsline {subsection}{\numberline {2.1}Methodology}{5}
\contentsline {subsubsection}{\numberline {2.1.1}Data Collection}{5}
\contentsline {subsubsection}{\numberline {2.1.2}User Study}{7}
\contentsline {subsubsection}{\numberline {2.1.3}Exit Survey}{9}
\contentsline {subsection}{\numberline {2.2}Results}{9}
\contentsline {subsubsection}{\numberline {2.2.1}Invisible Permission Requests}{10}
\contentsline {subsubsection}{\numberline {2.2.2}Request and Data Exposure Frequency}{11}
\contentsline {subsubsection}{\numberline {2.2.3}User Expectations}{12}
\contentsline {subsection}{\numberline {2.3}Analysis}{13}
\contentsline {subsubsection}{\numberline {2.3.1}Feasibility of Runtime Prompts}{13}
\contentsline {subsubsection}{\numberline {2.3.2}User Modeling}{14}
\contentsline {section}{\numberline {3}Android OS Instrumentation}{14}
\contentsline {subsection}{\numberline {3.1}Ensuring Coverage}{15}
\contentsline {subsubsection}{\numberline {3.1.1}Camera}{15}
\contentsline {subsubsection}{\numberline {3.1.2}Settings and Initialization}{16}
\contentsline {subsection}{\numberline {3.2}Side Channels}{17}
\contentsline {section}{\numberline {4}Performance Evaluation}{18}
\contentsline {subsection}{\numberline {4.1}Latency}{18}
\contentsline {subsection}{\numberline {4.2}Battery}{19}
\contentsline {section}{\numberline {5}Classifier}{19}
\contentsline {subsection}{\numberline {5.1}Features}{20}
\contentsline {subsubsection}{\numberline {5.1.1}Settings-Related Features}{20}
\contentsline {subsubsection}{\numberline {5.1.2}User Behavior Features}{21}
\contentsline {subsubsection}{\numberline {5.1.3}Runtime Features}{22}
\contentsline {subsection}{\numberline {5.2}Data Collection}{23}
\contentsline {subsection}{\numberline {5.3}Offline Model}{24}
\contentsline {subsubsection}{\numberline {5.3.1}Feature Selection}{24}
\contentsline {subsubsection}{\numberline {5.3.2}Model Evaluation}{25}
\contentsline {section}{\numberline {6}Related Work}{27}
\contentsline {section}{\numberline {7}Future Work}{27}
\contentsline {section}{\numberline {8}Conclusion}{28}
\contentsline {section}{\numberline {A}Appendix}{33}
